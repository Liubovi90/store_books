const express = require('express')
const passport = require('passport')
const router = express.Router()  // чтобы создавать примеры роутов

// @desc    Auth with Google
// @route   GET /auth/google
// Здесь мы проверяем, передаем данные о пользователе в функцию верификации
// Вообще, passport.authenticate() вызывает метод req.logIn автоматически, здесь же я указала это явно. Это добавляет удобство в отладке. Например, можно вставить сюда console.log(), чтобы посмотреть, что происходит...
// При удачной авторизации данные пользователя будут храниться в req.user
router.get('/google', passport.authenticate('google', { scope: ['profile'] }))

// @desc    Google auth callback
// @route   GET /auth/google/callback ==== проверка если пользоваель авторизирован в гугл аккаунте
router.get(
  '/google/callback',
  passport.authenticate('google', { failureRedirect: '/' }), // google - это стратегия, если ошибка отправлчет на главную
  (req, res) => {
    res.redirect('/dashboard') // если удачно
  }
)

// @desc    Logout user
// @route   /auth/logout
router.get('/logout', (req, res) => {
  req.logout()
  res.redirect('/')
})

module.exports = router
