const express = require('express')
const router = express.Router() // чтобы создавать примеры роутов
const { ensureAuth, ensureGuest } = require('../middleware/auth') // подключаю проверку на авторизацию

const Story = require('../models/Story')

// @desc    Login/Landing page
// @route   GET /
// для неавторизированного пользователя
router.get('/', ensureGuest, (req, res) => { // передаем файл login.hbs
  res.render('login', {
    layout: 'login',
  })
})

// @desc    Dashboard
// @route   GET /dashboard
// для авторизированного пользователя
router.get('/dashboard', ensureAuth, async (req, res) => { // получаем данные из файла dashboard.hbs
  try {
    const stories = await Story.find({ user: req.user.id }).lean()
    res.render('dashboard', {
      name: req.user.firstName,
      stories,
    })
  } catch (err) {
    console.error(err)
    res.render('error/500') // подключаемся к файлу error/500.hbs
  }
})

module.exports = router
