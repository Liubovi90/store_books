const moment = require('moment') // библиотека для работ с датами, чтобы форматировать дату

module.exports = {
  formatDate: function (date, format) { // форматирует дату
    return moment(date).utc().format(format)
  },
  // сокращает текст , показывает меньшую длину
  truncate: function (str, len) {
    if (str.length > len && str.length > 0) {
      let new_str = str + ' '
      new_str = str.substr(0, len)
      new_str = str.substr(0, new_str.lastIndexOf(' '))
      new_str = new_str.length > 0 ? new_str : str.substr(0, len)
      return new_str + '...'
    }
    return str
  },
  stripTags: function (input) {
    return input.replace(/<(?:.|\n)*?>/gm, '')
  },

  // подключаем иконку редактирования поста для авторизированного пользователя
  editIcon: function (storyUser, loggedUser, storyId, floating = true) {
    if (storyUser._id.toString() == loggedUser._id.toString()) {
      if (floating) {
        return `<a href="/stories/edit/${storyId}" class="btn-floating halfway-fab blue"><i class="fas fa-edit fa-small"></i></a>`
      } else {
        return `<a href="/stories/edit/${storyId}"><i class="fas fa-edit"></i></a>`
      }
    } else {
      return ''
    }
  },
  // настраиваем отбор public/private
  select: function (selected, options) {
    return options
      .fn(this)  //fn - это функция, которая передается в  качестве параметра
      .replace( // заменяем на регулярное выражение
        new RegExp(' value="' + selected + '"'),
        '$& selected="selected"'
      )
      .replace(
        new RegExp('>' + selected + '</option>'),
        ' selected="selected"$&'
      )
  },
}
