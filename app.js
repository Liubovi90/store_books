const path = require('path')
const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')  // чтобы читать enviroment variables с файла config.env
const morgan = require('morgan') // для регистрации запросов, автоматически формирует жерналы
const exphbs = require('express-handlebars')
const methodOverride = require('method-override') //чтобы переопределять запросы серверу 
const passport = require('passport')  // это middlewear для авторизации пользователей
const session = require('express-session')
const MongoStore = require('connect-mongo')(session)
const connectDB = require('./config/db')

// Load config
dotenv.config({ path: './config/config.env' }) // подключаем пакет dotenv

// Passport config
require('./config/passport')(passport) //  passport - этот параемтр указываем в passport.js

connectDB() // получаем в консоли, что MongoDB подключена

const app = express() // создаю приложение

// Body parser
app.use(express.urlencoded({ extended: false }))
app.use(express.json())

// Method override чтобы переопределять метод запроса к серверу 
app.use(
  methodOverride(function (req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
      // посмотри в urlencoded POST body и удали их
      let method = req.body._method
      delete req.body._method
      return method
    }
  })
)

// Logging
if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'))
}

// Handlebars Helpers - передаем все объекты из hbs.js
const {
  formatDate,
  stripTags,
  truncate,
  editIcon,
  select,
} = require('./helpers/hbs')

// Handlebars
app.engine(  // регистирую hbs в express для рендеринга страниц, hbs - его название
  '.hbs',
  exphbs({
    helpers: {
      formatDate,
      stripTags,
      truncate,
      editIcon,
      select,
    },
    defaultLayout: 'main',
    extname: '.hbs',
  })
)
app.set('view engine', '.hbs')  // указываем что по умолчанию используем hbs, view - это  ключ express

// Sessions
app.use(
  session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false,
    store: new MongoStore({ mongooseConnection: mongoose.connection }),
  })
)

// Passport middleware
// подключаем passport к express
app.use(passport.initialize())
app.use(passport.session())

// Set global var
app.use(function (req, res, next) {
  res.locals.user = req.user || null
  next()
})

// Static folder
app.use(express.static(path.join(__dirname, 'public'))) // подключаем все файлы с папки public

// Routes
app.use('/', require('./routes/index'))
app.use('/auth', require('./routes/auth'))
app.use('/stories', require('./routes/stories'))

const PORT = process.env.PORT || 3000

app.listen(
  PORT,
  console.log(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`)
)
